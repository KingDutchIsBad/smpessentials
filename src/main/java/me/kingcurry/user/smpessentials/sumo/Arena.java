package me.kingcurry.user.smpessentials.sumo;

import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public class Arena {
    public boolean started = false;
    private static Arena arena = null;
    public Arena() {
    }

    public static Arena getArena() {
        if (arena == null) {
            arena = new Arena();
        }
        return arena;
    }

    public int id = 0;
    public Location spawn = null;
    List<String> players = new ArrayList<String>();

    public Arena(Location loc, int id){
        this.spawn = loc;
        this.id = id;
    }

    public int getId(){
        return this.id;
    }

    public List<String> getPlayers(){
        return this.players;
    }
    public boolean getStarted() {
        return this.started;
    }
}
