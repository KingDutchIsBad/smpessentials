package me.kingcurry.user.smpessentials;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChatMsgUtil {
    public static void sendMessage(Player player , String message){
        player.sendMessage(ChatColor.translateAlternateColorCodes('&' , message));
    }
    public static void sendMessage(CommandSender player , String message){
        player.sendMessage(ChatColor.translateAlternateColorCodes('&' , message));
    }
    public static void sendTitle(Player player, String message , String messageSecond){
        player.sendTitle(ChatColor.translateAlternateColorCodes('&' , message) , ChatColor.translateAlternateColorCodes('&' , messageSecond));
    }
    public static void sendActionBar(Player player , String message){
        ChatMessageType chatMessageType = ChatMessageType.ACTION_BAR;
        player.spigot().sendMessage(chatMessageType , TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', message)));
    }
}
