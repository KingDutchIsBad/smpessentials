package me.kingcurry.user.smpessentials.recipes;

import me.kingcurry.user.smpessentials.SmpEssentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class Recipes {
    SmpEssentials plugin = SmpEssentials.getPlugin(SmpEssentials.class);

    public void godArmour() {
        ArrayList<String> lore = new ArrayList<>();
        ItemStack Chestplate = new ItemStack(Material.NETHERITE_CHESTPLATE);
        ItemStack Legggings = new ItemStack(Material.NETHERITE_LEGGINGS);
        ItemStack boots = new ItemStack(Material.NETHERITE_BOOTS);
        ItemStack helments = new ItemStack(Material.NETHERITE_HELMET);
        helments.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
        Chestplate.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
        Legggings.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
        boots.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
        ItemMeta itemMeta1 = Chestplate.getItemMeta();
        ItemMeta itemMeta2 = Legggings.getItemMeta();
        ItemMeta itemMeta3 = boots.getItemMeta();
        ItemMeta itemMeta4 = helments.getItemMeta();
        lore.add(ChatColor.DARK_RED + "---------------");
        lore.add(ChatColor.translateAlternateColorCodes('&',"&4Blood for the blood god" ));
        lore.add(ChatColor.DARK_RED + "---------------");
        itemMeta1.setLore(lore);
        itemMeta2.setLore(lore);
        itemMeta3.setLore(lore);
        itemMeta4.setLore(lore);
        itemMeta1.setDisplayName(ChatColor.translateAlternateColorCodes('&',"&4Blood God Armour"));
        itemMeta3.setDisplayName(ChatColor.translateAlternateColorCodes('&',"&4Blood God Armour"));
        itemMeta2.setDisplayName(ChatColor.translateAlternateColorCodes('&',"&4Blood God Armour"));
        itemMeta4.setDisplayName(ChatColor.translateAlternateColorCodes('&',"&4Blood God Armour"));
        Chestplate.setItemMeta(itemMeta1);
        Legggings.setItemMeta(itemMeta2);
        boots.setItemMeta(itemMeta3);
        helments.setItemMeta(itemMeta4);
        NamespacedKey ChestPlate2 = new NamespacedKey(plugin, "ChesPlate_KEY");
        ShapedRecipe plate = new ShapedRecipe(ChestPlate2, Chestplate);
        plate.shape("GGG" , "GAG" , "GGG");
        plate.setIngredient('G' , Material.GOLD_BLOCK);
        plate.setIngredient('A' , Material.NETHERITE_CHESTPLATE);
        NamespacedKey Leggings2 = new NamespacedKey(plugin,"Leg_KEY" );
        ShapedRecipe leg = new ShapedRecipe(Leggings2 , Legggings);
        leg.shape("GGG" , "GLG" , "GGG");
        leg.setIngredient('G' , Material.GOLD_BLOCK);
        leg.setIngredient('L' , Material.NETHERITE_LEGGINGS);
        NamespacedKey Boots2 = new NamespacedKey(plugin, "Boots_Key");
        ShapedRecipe boot = new ShapedRecipe(Boots2 , boots );
        boot.shape("GGG" , "GBG" , "GGG");
        boot.setIngredient('G' , Material.GOLD_BLOCK);
        boot.setIngredient('B' , Material.NETHERITE_BOOTS);
        NamespacedKey helmet = new NamespacedKey(plugin , "Helmet_Key");
        ShapedRecipe helmet2 = new ShapedRecipe(helmet , helments);
        helmet2.shape("GGG" , "GHG" ,"GGG");
        helmet2.setIngredient('G' , Material.GOLD_BLOCK);
        helmet2.setIngredient('H' , Material.NETHERITE_HELMET);
        if (plugin.getConfig().getBoolean("Armourofgod") && Bukkit.getServer().getRecipe(Boots2) == null
                && Bukkit.getServer().getRecipe(helmet) == null
                && Bukkit.getServer().getRecipe(Leggings2) == null
                && Bukkit.getServer().getRecipe(ChestPlate2) == null) {
            plugin.getServer().addRecipe(plate);
            plugin.getServer().addRecipe(leg);
            plugin.getServer().addRecipe(boot);
            plugin.getServer().addRecipe(helmet2);
        }
    }

    public void backPack() {
        ItemStack itemStack = new ItemStack(Material.WITHER_SKELETON_SKULL);
        ItemMeta itemMeta = itemStack.getItemMeta();
        if (itemMeta != null) itemMeta.setDisplayName(net.md_5.bungee.api.ChatColor.GOLD + "Backpack");
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.GOLD + "-----------------------");
        lore.add(ChatColor.GOLD + "Backpack");
        lore.add(ChatColor.GOLD + "-----------------------");
        if (itemMeta != null) itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        NamespacedKey namespacedKey = new NamespacedKey(plugin, "Backpack_key");
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey, itemStack);
        shapedRecipe.shape("GGG", "GHG", "GGG");
        shapedRecipe.setIngredient('G', Material.GOLD_INGOT);
        shapedRecipe.setIngredient('H', Material.WITHER_SKELETON_SKULL);
        if (plugin.getConfig().getBoolean("Backpack.craft") && Bukkit.getServer().getRecipe(namespacedKey) == null) {
            plugin.getServer().addRecipe(shapedRecipe);
        }

    }

    public void sword() {
        ItemStack im = new ItemStack(Material.NETHERITE_SWORD);
        ItemMeta itemMeta = im.getItemMeta();
        itemMeta.setDisplayName(ChatColor.DARK_AQUA + "Ancient Sword");
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.DARK_AQUA + "--------------------------------------------");
        lore.add(ChatColor.AQUA + "Ancient Sword");
        lore.add(ChatColor.AQUA + "This sword will make the entity you hit bleed");
        lore.add(ChatColor.DARK_AQUA + "--------------------------------------------");
        itemMeta.setLore(lore);
        im.setItemMeta(itemMeta);
        NamespacedKey sword = new NamespacedKey(plugin, "AncientSword");
        ShapedRecipe swordrec = new ShapedRecipe(sword, im);
        swordrec.shape("NNN", "NSN", "NNN");
        swordrec.setIngredient('N', Material.NETHERITE_SCRAP);
        swordrec.setIngredient('S', Material.NETHERITE_SWORD);
        if (plugin.getConfig().getBoolean("AncientSword.enable") && Bukkit.getServer().getRecipe(sword) == null) {
            Bukkit.addRecipe(swordrec);
        }
    }

    public void craftingTable() {
        ItemStack itemStack = new ItemStack(Material.PLAYER_HEAD);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(ChatColor.GREEN + "Portable Crafting Table");
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.DARK_GREEN + "Portable Crafting Table");
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        NamespacedKey namespacedKey = new NamespacedKey(plugin, "Crafting");
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey, itemStack);
        shapedRecipe.shape("CCC", "CCC", "CCC");
        shapedRecipe.setIngredient('C', Material.CRAFTING_TABLE);
        if (plugin.getConfig().getBoolean("Crafting Table.enable") && Bukkit.getServer().getRecipe(namespacedKey) == null) {
            plugin.getServer().addRecipe(shapedRecipe);
        }
    }

    public void healthStick() {
        ItemStack itemStack = new ItemStack(Material.STICK);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(ChatColor.GOLD + "Show Health Stick");
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.YELLOW + "--------------------------------------");
        lore.add(ChatColor.GOLD + "This stick shows the enemy's health");
        lore.add(ChatColor.GOLD + "after you hit them");
        lore.add(ChatColor.YELLOW + "--------------------------------------");
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        NamespacedKey namespacedKey = new NamespacedKey(plugin, "Health_Key");
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey, itemStack);
        shapedRecipe.shape("SSS", "SSS", "SSS");
        shapedRecipe.setIngredient('S', Material.STICK);
        if (plugin.getConfig().getBoolean("Stick.enable") && Bukkit.getServer().getRecipe(namespacedKey) == null) {
            plugin.getServer().addRecipe(shapedRecipe);
        }
    }

    public void launcher() {
        List<String> lore = new ArrayList<String>();
        lore.add(ChatColor.BLUE + "----------");
        lore.add(ChatColor.DARK_AQUA + "Launcher which launches you high up in the air");
        lore.add(ChatColor.BLUE + "----------");
        ItemStack feather = new ItemStack(Material.FEATHER);
        ItemMeta itemMeta = feather.getItemMeta();
        itemMeta.setLore(lore);
        itemMeta.setDisplayName(ChatColor.DARK_AQUA + "Launcher");
        feather.setItemMeta(itemMeta);
        ShapedRecipe shapedRecipe = new ShapedRecipe(new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "LaunchPad_NamesKey"), feather);
        shapedRecipe.shape("BBB", "BFB", "BBB");
        shapedRecipe.setIngredient('B', Material.BLAZE_ROD);
        shapedRecipe.setIngredient('F', Material.FEATHER);
        if (SmpEssentials.getPlugin(SmpEssentials.class).getConfig().getBoolean("Launcher.enable")) {
            if (Bukkit.getServer().getRecipe(new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "LaunchPad_NamesKey")) == null) {
                Bukkit.addRecipe(shapedRecipe);
            }
        }
    }

    public void glowStick() {
        ItemStack GlowStick = new ItemStack(Material.STICK);
        ItemMeta itemMeta = GlowStick.getItemMeta();
        itemMeta.setDisplayName(ChatColor.GOLD + "GlowStick");
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.GOLD + "Glow Effect I");
        itemMeta.setLore(lore);
        GlowStick.setItemMeta(itemMeta);
        NamespacedKey namespacedKey = new NamespacedKey(plugin, "Glow_Stick");
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey, GlowStick);
        shapedRecipe.shape("   ", "RSR", "   ");
        shapedRecipe.setIngredient('R', Material.BLAZE_ROD);
        shapedRecipe.setIngredient('S', Material.STICK);
        if (plugin.getConfig().getBoolean("GlowStick") && Bukkit.getServer().getRecipe(namespacedKey) == null) {
            plugin.getServer().addRecipe(shapedRecipe);
        }

    }

    public void explosivePick() {
        ItemStack itemStack = new ItemStack(Material.NETHERITE_PICKAXE);
        ItemMeta itemMeta = itemStack.getItemMeta();
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.RED + "Explosive Break I");
        itemMeta.addEnchant(SmpEssentials.getCustomEnchantments(), 1, true);
        itemMeta.setLore(lore);
        itemMeta.setDisplayName("Explosion Pickaxe");
        itemStack.setItemMeta(itemMeta);
        NamespacedKey namespacedKey = new NamespacedKey(plugin, "Pickaxe_Explosion");
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey, itemStack);
        shapedRecipe.shape("TTT", "TNT", "TTT");
        shapedRecipe.setIngredient('T', Material.TNT);
        shapedRecipe.setIngredient('N', Material.NETHERITE_PICKAXE);
        if (plugin.getConfig().getBoolean("ExplosionPickaxe") && Bukkit.getServer().getRecipe(namespacedKey) == null) {
            Bukkit.addRecipe(shapedRecipe);
        }
    }

    public void telepathyShovel() {
        ItemStack itemStack = new ItemStack(Material.NETHERITE_SHOVEL);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(ChatColor.DARK_AQUA + "Telepathy Shovel");
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.AQUA + "---------------------------------");
        lore.add(ChatColor.BLUE + "Telepathy Shovel");
        lore.add(ChatColor.AQUA + "---------------------------------");
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);
        NamespacedKey namespacedKey = new NamespacedKey(plugin, "Telepathy_Shovel");
        ShapedRecipe recipe = new ShapedRecipe(namespacedKey, itemStack);
        recipe.shape("GGG", "GSG", "GGG");
        recipe.setIngredient('G', Material.GOLD_INGOT);
        recipe.setIngredient('S', Material.NETHERITE_SHOVEL);
        if (plugin.getConfig().getBoolean("Telepathy Shovel.enable") && Bukkit.getServer().getRecipe(namespacedKey) == null) {
            plugin.getServer().addRecipe(recipe);
        }
    }

    public void bow() {
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.BLUE + "---------------");
        lore.add(ChatColor.BLUE + "Teleport Bow");
        lore.add(ChatColor.BLUE + "---------------");
        ItemStack itemStack = new ItemStack(Material.BOW);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setLore(lore);
        itemMeta.setDisplayName(ChatColor.DARK_BLUE + "Teleport Bow");
        itemStack.setItemMeta(itemMeta);
        NamespacedKey Bow = new NamespacedKey(plugin, "Teleport_BOW");
        ShapedRecipe telebow = new ShapedRecipe(Bow, itemStack);
        telebow.shape("NNN", "NBN", "NNN");
        telebow.setIngredient('N', Material.NETHERITE_INGOT);
        telebow.setIngredient('B', Material.BOW);
        itemStack.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 2);
        if (plugin.getConfig().getBoolean("TeleportBow") && Bukkit.getServer().getRecipe(Bow) == null) {
            plugin.getServer().addRecipe(telebow);
        }


    }

    public void lighteningAxe() {
        ArrayList<String> lore = new ArrayList<>();
        ItemStack GodAxe = new ItemStack(Material.NETHERITE_AXE);
        ItemMeta itemMeta = GodAxe.getItemMeta();
        itemMeta.setDisplayName(ChatColor.DARK_RED + "God AXE");
        lore.add(ChatColor.RED + "---------------");
        lore.add("This Axe will kill anyone");
        lore.add(ChatColor.RED + "---------------");
        itemMeta.setLore(lore);
        GodAxe.setItemMeta(itemMeta);
        NamespacedKey namespacedKey = new NamespacedKey(plugin, "Zeus_AXE");
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey, GodAxe);
        shapedRecipe.shape("NNN", "NIN", "NNN");
        shapedRecipe.setIngredient('N', Material.NETHERITE_INGOT);
        shapedRecipe.setIngredient('I', Material.IRON_AXE);
        GodAxe.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 10);
        if (plugin.getConfig().getBoolean("LightningAxe") && Bukkit.getServer().getRecipe(namespacedKey) == null) {
            plugin.getServer().addRecipe(shapedRecipe);
        }

    }

    public void tridentRecipe() {
        ItemStack itemStack = new ItemStack(Material.TRIDENT);
        SmpEssentials plugin = SmpEssentials.getPlugin(SmpEssentials.class);
        NamespacedKey namespacedKey = new NamespacedKey(plugin, "Trident_Recipe");
        ShapedRecipe shapedRecipe = new ShapedRecipe(namespacedKey, itemStack);
        shapedRecipe.shape("III", "GSG", " D ");
        shapedRecipe.setIngredient('I', Material.IRON_INGOT);
        shapedRecipe.setIngredient('G', Material.GOLD_INGOT);
        shapedRecipe.setIngredient('S', Material.STICK);
        shapedRecipe.setIngredient('D', Material.DIAMOND);
        if (plugin.getConfig().getBoolean("Trident.enable") && Bukkit.getServer().getRecipe(namespacedKey) == null) {
            plugin.getServer().addRecipe(shapedRecipe);
        }
    }
}
