package me.kingcurry.user.smpessentials.commands;

import me.kingcurry.user.smpessentials.SmpEssentials;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Vanish implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
            if (sender.hasPermission("SmpEssentials.command.vanish")) {
                if (sender instanceof Player) {
                    Player player = (Player) sender;
                    if (SmpEssentials.getPlayersVanished().contains(player)) {
                        SmpEssentials.getPlayersThatJumped().remove(player);
                        for (Player online : Bukkit.getOnlinePlayers()) {
                            online.showPlayer(SmpEssentials.getPlugin(SmpEssentials.class), player);
                        }
                        player.sendMessage(ChatColor.DARK_AQUA + "You are now not Invisible");
                    } else {
                        for (Player online : Bukkit.getOnlinePlayers()) {
                            if (!online.isOp() || !online.hasPermission("SmpEssentials.vanish.seeOther")) {
                                online.hidePlayer(SmpEssentials.getPlugin(SmpEssentials.class), player);
                            }
                        }
                        SmpEssentials.getPlayersVanished().add(player);
                        player.sendMessage(ChatColor.DARK_AQUA + "You are now  Invisible");
                    
                    }
                }
            }else {
                sender.sendMessage(ChatColor.RED+ "You don't have the permission to use this.");
            }
        return true;
    }
}
