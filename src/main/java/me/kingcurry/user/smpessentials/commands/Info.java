package me.kingcurry.user.smpessentials.commands;

import me.kingcurry.user.smpessentials.ChatMsgUtil;
import me.kingcurry.user.smpessentials.SmpEssentials;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Info implements CommandExecutor {
    SmpEssentials plugin = SmpEssentials.getPlugin(SmpEssentials.class);
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
            if(args.length <= 0){
                TextComponent textComponent = new TextComponent(ChatColor.AQUA + "_KingCurry_(Dev)");
                textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/members/kingdutchisbad.817692/"));
                textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(ChatColor.AQUA + "Author")));
                sender.sendMessage(ChatColor.DARK_AQUA + "-------------------------------------");
                sender.sendMessage(ChatColor.AQUA + "This Plugin is made by:");
                sender.spigot().sendMessage(textComponent);
                sender.sendMessage(ChatColor.AQUA + "The plugin adds custom recipes");
                sender.sendMessage(ChatColor.AQUA + "Commands:");
                sender.sendMessage(ChatColor.BLUE + "/backpack");
                sender.sendMessage(ChatColor.BLUE + "/SmpEssentials");
                sender.sendMessage(ChatColor.BLUE + "/SmpEssentials reload");
                sender.sendMessage(ChatColor.BLUE + "/vanishtest");
                sender.sendMessage(ChatColor.BLUE + "/rtp");
                sender.sendMessage(ChatColor.BLUE + "/Sumo join");
                sender.sendMessage(ChatColor.BLUE + "/Sumo Leave");
                sender.sendMessage(ChatColor.BLUE + "/Sumo Remove");
                sender.sendMessage(ChatColor.BLUE + "/Sumo add");
                ChatMsgUtil.sendMessage(sender, ChatColor.BLUE + "/SmpEssentials reload");
                TextComponent textComponent1 = new TextComponent(ChatColor.AQUA + "Download Link!");
                textComponent1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(ChatColor.AQUA + "Click to Download now!")));
                textComponent1.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/resources/SmpEssentials.81998/"));
                sender.spigot().sendMessage(textComponent1);
                TextComponent textComponent2 = new TextComponent(ChatColor.AQUA + "Source Code!");
                textComponent2.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://bitbucket.org/KingDutchIsBad/smpessentials/src/master/"));
                textComponent2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(ChatColor.AQUA + "Click to View the source code")));
                sender.spigot().sendMessage(textComponent2);
                sender.sendMessage(ChatColor.DARK_AQUA + "-------------------------------------");
            }
            if(args.length == 1){
                if(args[0].equalsIgnoreCase("reload")){
                    if(sender.hasPermission("SmpEssentials.use.reload")){
                        plugin.reloadConfig();
                        plugin.saveConfig();
                        SmpEssentials.reload();
                        SmpEssentials.save();
                        plugin.getServer().getPluginManager().disablePlugin(plugin);
                        plugin.getServer().getPluginManager().enablePlugin(plugin);
                        sender.sendMessage(org.bukkit.ChatColor.RED + "[Smp" + org.bukkit.ChatColor.DARK_RED + "Esse" + org.bukkit.ChatColor.DARK_AQUA + "ntials]" + org.bukkit.ChatColor.AQUA + "Plugin reloaded");
                    }
                }
            }

    return true;
    }
}
