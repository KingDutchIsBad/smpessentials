package me.kingcurry.user.smpessentials.commands;

import me.kingcurry.user.smpessentials.ChatMsgUtil;
import me.kingcurry.user.smpessentials.SmpEssentials;
import me.kingcurry.user.smpessentials.TeleportUtils;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class Rtp implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            FileConfiguration config = SmpEssentials.getPlugin(SmpEssentials.class).getConfig();
            if(sender.hasPermission("SmpEssentials.use.teleport")){
                if(config.getBoolean("RTP.Price.Enabled")){
                    Player player = (Player) sender;
                    player.teleport(TeleportUtils.findSafeLocation(player));
                    player.playSound(player.getLocation(),
                            Sound.ENTITY_ENDER_PEARL_THROW , 1, 0.1f);
                    SmpEssentials.getEconomy().withdrawPlayer(player , config.getDouble("RTP.Price.Price"));
                    ChatMsgUtil.sendMessage(player,
                            "&3Wooosh");
                }
            }
        }
        return true;
    }
}
