package me.kingcurry.user.smpessentials;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Random;

public class TeleportUtils {
    private static final SmpEssentials PLUGIN = SmpEssentials.getPlugin(SmpEssentials.class);

    private static final HashSet<Material> BAD_BLOCKS = new HashSet<>();

    static {
        BAD_BLOCKS.add(Material.LAVA);
        BAD_BLOCKS.add(Material.FIRE);
        BAD_BLOCKS.add(Material.CACTUS);
        BAD_BLOCKS.add(Material.WATER);
    }

    private static Location generateLocation(Player player) {
        Random random = new Random();
        int x = 0;
        int z = 0;
        int y = 0;
        if (PLUGIN.getConfig().getBoolean("world-border")) {
            x = random.nextInt(PLUGIN.getConfig().getInt("RTP.border"));
            z = random.nextInt(PLUGIN.getConfig().getInt("RTP.border"));
            y = 150;
        } else if (!PLUGIN.getConfig().getBoolean("RTP.world-border")) {
            x = random.nextInt(25000);
            z = random.nextInt(25000);
            y = 150;
        }
        Location randomLocation = new Location(player.getWorld(), x, y, z);
        y = randomLocation.getWorld().getHighestBlockYAt(randomLocation);
        randomLocation.setY(y);
        return randomLocation;
    }

    public static Location findSafeLocation(Player player){
        Location randomLocation = generateLocation(player);
        while (!isLocationSafe(randomLocation)){
            randomLocation = generateLocation(player);
        }
        return randomLocation;
    }

    private static boolean isLocationSafe(Location location) {
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();
        Block block = location.getWorld().getBlockAt(x, y, z);
        Block below = location.getWorld().getBlockAt(x, y - 1, z);
        Block above = location.getWorld().getBlockAt(x, y + 1, z);
        return !(BAD_BLOCKS.contains(below.getType())) || (block.getType().isSolid()) || (above.getType().isSolid());
    }
}
