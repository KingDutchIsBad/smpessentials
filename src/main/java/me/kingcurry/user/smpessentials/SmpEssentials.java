package me.kingcurry.user.smpessentials;

import me.kingcurry.user.smpessentials.commands.Backpack;
import me.kingcurry.user.smpessentials.commands.Info;
import me.kingcurry.user.smpessentials.commands.Rtp;
import me.kingcurry.user.smpessentials.commands.Vanish;
import me.kingcurry.user.smpessentials.events.*;
import me.kingcurry.user.smpessentials.recipes.Recipes;
import me.kingcurry.user.smpessentials.sumo.ArenaManager;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.NamespacedKey;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class SmpEssentials extends JavaPlugin implements CommandExecutor, TabCompleter {
    private static CustomEnchantments customEnchantments;
    private static HashSet<Player> playersVanished;
    private static ArrayList<Player> playersThatJumped = null;
    private static File file;
    private static FileConfiguration customFile;
    private static Economy econ;

    public static CustomEnchantments getCustomEnchantments() {
        return customEnchantments;
    }

    public boolean vault;

    public static ArrayList<Player> getPlayersThatJumped() {
        if (playersThatJumped == null) {
            playersThatJumped = new ArrayList<>();
        }
        return playersThatJumped;
    }

    public static HashSet<Player> getPlayersVanished() {
        if (playersVanished == null) {
            playersVanished = new HashSet<>();
        }
        return playersVanished;
    }


    public static Economy getEconomy() {
        return econ;
    }

    public static void setup() {
        file = new File(Bukkit.getServer().getPluginManager().getPlugin("SmpEssentials").getDataFolder(), "data.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                //owww
            }
        }
        customFile = YamlConfiguration.loadConfiguration(file);
    }

    public static FileConfiguration get() {
        return customFile;
    }

    public static void save() {
        try {
            customFile.save(file);
        } catch (IOException e) {
            System.out.println("Couldn't save file");
        }
    }

    public static void reload() {
        customFile = YamlConfiguration.loadConfiguration(file);
    }

    @Override
    public void onEnable() {
        vault = Bukkit.getServer().getPluginManager().getPlugin("Vault") != null;
        int pluginId = 11169;
        new Metrics(this, pluginId);
        getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', "&c[Smp&4Essen&3tials] &9Made by &3_KingCurry_ "));
        customEnchantments = new CustomEnchantments("Explosive_key");
        Recipes recipes = new Recipes();
        recipes.godArmour();
        recipes.backPack();
        recipes.bow();
        recipes.craftingTable();
        recipes.backPack();
        recipes.glowStick();
        recipes.explosivePick();
        recipes.launcher();
        recipes.lighteningAxe();
        recipes.sword();
        recipes.healthStick();
        recipes.telepathyShovel();
        recipes.tridentRecipe();
        getServer().getPluginManager().registerEvents(new EntityDam(), this);
        getServer().getPluginManager().registerEvents(new Interact(), this);
        getServer().getPluginManager().registerEvents(new Projectile(), this);
        getServer().getPluginManager().registerEvents(new Walk(), this);
        getServer().getPluginManager().registerEvents(new Join(), this);
        getServer().getPluginManager().registerEvents(new Inventory(), this);
        getServer().getPluginManager().registerEvents(new BlockPlace(), this);
        getCommand("Backpack").setExecutor(new Backpack());
        getCommand("SmpEssentials").setExecutor(new Info());
        getCommand("vanish").setExecutor(new Vanish());
        getCommand("rtp").setExecutor(new Rtp());
        getCommand("sumo").setExecutor(this);
        new ArenaManager(this);
        getConfig().options().copyDefaults();
        saveDefaultConfig();
        registerEnchantment(customEnchantments);
        setup();
        save();
        ArenaManager.getManager().loadGames();

    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return true;
    }

    public void registerEnchantment(Enchantment enchantment) {
        boolean registered = true;
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
            Enchantment.registerEnchantment(enchantment);
        } catch (Exception e) {
            registered = false;
            getLogger().severe(e.getStackTrace().toString());
        }
        if (registered) {
            System.out.println("Enchantment Registered");
        }
    }

    @Override
    public void onDisable() {
        try {
            Field keyField = Enchantment.class.getDeclaredField("byKey");
            keyField.setAccessible(true);
            @SuppressWarnings("unchecked")
            HashMap<NamespacedKey, Enchantment> byKey = (HashMap<NamespacedKey, Enchantment>) keyField.get(null);
            byKey.remove(customEnchantments.getKey());
            Field nameField = Enchantment.class.getDeclaredField("byName");
            nameField.setAccessible(true);
            @SuppressWarnings("unchecked")
            HashMap<String, Enchantment> byName = (HashMap<String, Enchantment>) nameField.get(null);
            byName.remove(customEnchantments.getName());
        } catch (Exception e) {
            // deepcode ignore dontUsePrintStackTrace: MEH let it print the stack trace
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            ChatMsgUtil.sendMessage(sender, "&4You have to be a player to use this command");
        }
        if (args.length == 0) {
            ChatMsgUtil.sendMessage(sender, "&4Sorry but there has to be a argument after /sumo ");
            ChatMsgUtil.sendMessage(sender, "&4Use /Sumo join or /Sumo leave ");
        }
        if (sender instanceof Player && command.getName().equalsIgnoreCase("sumo")) {
            Player player = (Player) sender;
            if (player.hasPermission("SmpEssentials.sumo.admin")) {
                if (args.length == 1 && args[0].equalsIgnoreCase("add")) {
                    ArenaManager.getManager().createArena(player.getLocation());
                    ChatMsgUtil.sendMessage(player, "&6[SmpEssentials] &3Arena added at " + player.getLocation());
                }
            }
            if (args.length == 2 && args[0].equalsIgnoreCase("join")
                    && player.hasPermission("SmpEssentials.sumo.join")) {
                int number = Integer.parseInt(args[1]);
                if (ArenaManager.getManager().getArena(number) != null) {
                    if (!ArenaManager.getManager().isInGame(player)) {
                        if (ArenaManager.getManager().getArena(number).getPlayers().size() < 2) {
                            ArenaManager.getManager().addPlayer(player, number);
                            player.setGameMode(GameMode.ADVENTURE);
                        } else {
                            player.sendMessage("Already full");
                        }
                    } else {
                        player.sendMessage("You are already in a game");
                    }
                } else {
                    player.sendMessage("That Arena Doesn't exist");
                }
            }

            if (args.length == 2 && sender.hasPermission("SmpEssentials.sumo.admin")
                    && args[0].equalsIgnoreCase("remove")) {
                if (args[1] == null) return false;
                int number = Integer.parseInt(args[1]);
                if (get().getIntegerList("Arenas.Arenas").contains(number)) {
                    ArenaManager.getManager().removeArena(number);
                } else {
                    ChatMsgUtil.sendMessage(player, "&4That arena doesn't exist");
                }

            }
            if (args.length == 1 && args[0].equalsIgnoreCase("leave")) {
                if (ArenaManager.getManager().isInGame(player)) {
                    ArenaManager.getManager().removePlayer(player);
                    ChatMsgUtil.sendMessage(player, "&6You have successfully left");
                } else {
                    ChatMsgUtil.sendMessage(player, "&6You're not in a game.");
                }

            }

        }
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        List<String> completionsArgs1 = new ArrayList<>();
        if (sender.hasPermission("SmpEssentials.sumo.admin")) {
            completionsArgs1.add("add");
            completionsArgs1.add("remove");
        }
        completionsArgs1.add("leave");
        completionsArgs1.add("join");
        if (command.getName().equalsIgnoreCase("Sumo")) {
            if (args.length == 1) {
                return completionsArgs1;
            }
        }
        return null;
    }
}

