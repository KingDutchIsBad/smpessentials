package me.kingcurry.user.smpessentials.events;

import me.kingcurry.user.smpessentials.Bleed;
import me.kingcurry.user.smpessentials.ChatMsgUtil;
import me.kingcurry.user.smpessentials.LifeSteal;
import me.kingcurry.user.smpessentials.SmpEssentials;
import me.kingcurry.user.smpessentials.sumo.Arena;
import me.kingcurry.user.smpessentials.sumo.ArenaManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.*;

public class EntityDam implements Listener {

    List<Entity> passiveMobs = new ArrayList<>();

    @EventHandler
    public void above(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (event.getDamager() instanceof Player) {
                Player player1 = (Player) event.getDamager();
                if (player.getInventory().getHelmet() != null && player.getInventory().getHelmet().getItemMeta() != null)
                    if (player1.getInventory().getHelmet().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&4Blood God Armour"))) {
                        if (!player.getActivePotionEffects().contains(new PotionEffect(PotionEffectType.POISON, 300, 2))) {
                            player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 300, 2));
                        }
                    }
            }
            if (player.getInventory().getChestplate() == null) return;
            else if (player.getInventory().getChestplate().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&4Blood God Armour"))) {
                player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 300, 2));
            }
        }
        if(event.getDamager() instanceof Player) {
            Player player = (Player) event.getDamager();
            if (!nullchecks(event)) return;
            if (player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(ChatColor.GOLD + "Glow Effect I")) {
                event.getEntity().setGlowing(true);
            }
            if (event.getEntity() instanceof LivingEntity) {
                LivingEntity victim = (LivingEntity) event.getEntity();
                if (player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(ChatColor.AQUA + "Ancient Sword")) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c[Smp&4Essent&3ials] ") + ChatColor.DARK_RED + "Your Target is now bleeding.");
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c[Smp&4Essent&3ials] ") + ChatColor.DARK_RED + "Sometimes the target will have some health left and will not die.");
                    new Bleed(victim).runTaskTimer(SmpEssentials.getPlugin(SmpEssentials.class), 0L, 40L);
                }
            }
            if (SmpEssentials.getPlugin(SmpEssentials.class).getConfig().getBoolean("Stick.playeruse")) {
                if (!(event.getEntity() instanceof LivingEntity)) return;
                if (player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(ChatColor.GOLD + "This stick shows the enemy's health")) {
                    LivingEntity livingEntity = (LivingEntity) event.getEntity();
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c[Smp&4Essent&3ials] ") + ChatColor.DARK_AQUA
                            + "The damaged Entity's health is " + livingEntity.getHealth());
                }
            }
        }
        Boolean enabled = true;
        if (!enabled) {
            NamespacedKey namespacedKeys = new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "ShrineBosses");
            if (nullchecks(event)) {
                Player player = (Player) event.getDamager();
                PersistentDataContainer persistentDataContainerItemInHand = player.getInventory().getItemInMainHand().getItemMeta().getPersistentDataContainer();
                if (persistentDataContainerItemInHand.has(namespacedKeys, PersistentDataType.INTEGER)
                        && persistentDataContainerItemInHand.get(namespacedKeys, PersistentDataType.INTEGER).equals(940454)) {
                    new Bleed((LivingEntity) event.getEntity()).runTaskTimer(SmpEssentials.getPlugin(SmpEssentials.class), 0L, 100L);
                }
                if (persistentDataContainerItemInHand.has(namespacedKeys,
                        PersistentDataType.INTEGER) && persistentDataContainerItemInHand.get(namespacedKeys,
                        PersistentDataType.INTEGER).equals(23121313)) {
                    event.getEntity().setVelocity(new Vector(2, 2, 0));
                }

            }
            if (event.getDamager() instanceof Player) {
                Player player = (Player) event.getDamager();
                if (player.getInventory().getHelmet() != null) {
                    if (player.getInventory().getHelmet().getItemMeta().getPersistentDataContainer().has(namespacedKeys,
                            PersistentDataType.INTEGER)
                            && player.getInventory().getHelmet().getItemMeta().getPersistentDataContainer().get(namespacedKeys, PersistentDataType.INTEGER).equals(54173)
                            && (int) (Math.random() * 3) == 1) {
                        new LifeSteal((LivingEntity) event.getEntity(), (LivingEntity) event.getDamager()).runTaskTimer(SmpEssentials.getPlugin(SmpEssentials.class), 0L, 1L);
                    }
                }
                if (player.getInventory().getItemInMainHand().getType().equals(Material.END_ROD)) {
                    int random = (int) (Math.random() * 2);
                    if (random == 1) {
                        for (int i = 1; i < 6; i++) {
                            Illusioner entity = (Illusioner) event.getDamager().getWorld().spawnEntity(player.getLocation(), EntityType.ILLUSIONER);
                            passiveMobs.add(entity);
                            entity.setTarget((LivingEntity) event.getEntity());
                        }
                    }
                }
            }
        }

    }

    @EventHandler
    public void onTarget(EntityTargetLivingEntityEvent event) {
        if (passiveMobs.contains(event.getEntity())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void launchPadStuff(EntityDamageEvent event) {
        if (event.getEntity().getType().equals(EntityType.PLAYER)) {
            if (event.getCause().equals(EntityDamageEvent.DamageCause.FALL) && SmpEssentials.getPlayersThatJumped().contains((Player) event.getEntity())) {
                SmpEssentials.getPlayersThatJumped().remove((Player) event.getEntity());
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void guardianStuff(EntityDeathEvent event) {
        FileConfiguration config = SmpEssentials.getPlugin(SmpEssentials.class).getConfig();

        NamespacedKey namespacedKey = new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "Guardian");

        ConfigurationSection configSection = config.getConfigurationSection("CustomGuardian");
        if (configSection != null) {
            NamespacedKey namespacedKeys = new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "ShrineBosses");
            PersistentDataContainer p = event.getEntity().getPersistentDataContainer();
            Map<Enchantment, Integer> swordMap = new HashMap<>();
            swordMap.put(Enchantment.DURABILITY, 10);
            swordMap.put(Enchantment.DAMAGE_ALL, 10);
            swordMap.put(Enchantment.DAMAGE_ARTHROPODS, 10);
            swordMap.put(Enchantment.SWEEPING_EDGE, 10);
            swordMap.put(Enchantment.MENDING, 10);
            Map<Enchantment, Integer> bowMap = new HashMap<>();
            bowMap.put(Enchantment.ARROW_DAMAGE, 10);
            bowMap.put(Enchantment.ARROW_FIRE, 10);
            bowMap.put(Enchantment.ARROW_INFINITE, 10);
            bowMap.put(Enchantment.ARROW_KNOCKBACK, 10);
            bowMap.put(Enchantment.MENDING, 10);
            bowMap.put(Enchantment.DURABILITY, 10);
            Map<Enchantment, Integer> helmetMap = new HashMap<>();
            helmetMap.put(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
            helmetMap.put(Enchantment.PROTECTION_EXPLOSIONS, 10);
            helmetMap.put(Enchantment.MENDING, 10);
            helmetMap.put(Enchantment.DURABILITY, 10);
            ArrayList<String> swordLore = new ArrayList<>();
            swordLore.add(ChatColor.translateAlternateColorCodes('&', "&6This is the god sword it is the most powerful specimen in this world it launches your enemy up "));
            swordLore.add(ChatColor.translateAlternateColorCodes('&',
                    "&6in the air whenever you hit your enemy"));
            ArrayList<String> bowLore = new ArrayList<>();
            bowLore.add(ChatColor.translateAlternateColorCodes('&', "&6This is the bow of the Sand Protector it is the best bow humans have ever seen it bleeds"));
            bowLore.add(ChatColor.translateAlternateColorCodes('&', " &4away your enemy whenever a shot is landed"));
            ArrayList<String> helmetLore = new ArrayList<>();
            helmetLore.add(ChatColor.translateAlternateColorCodes('&', "&6This is the Helmet of God of Conflict and wars it takes health away from your enemy and give it to"));
            helmetLore.add(ChatColor.translateAlternateColorCodes('&', " &6you for 5 secs everytime you hit your enemy"));

            bosses(event, namespacedKeys, p, "The God Sword", Material.NETHERITE_SWORD, swordMap, swordLore, EntityType.ILLUSIONER, 12216, 23121313);
            bosses(event, namespacedKeys, p, "The SandStone bow", Material.BOW, bowMap, bowLore, EntityType.SKELETON, 3341212, 940454);
            bosses(event, namespacedKeys, p, "The Helmet of the God of wars and conflict", Material.NETHERITE_HELMET, helmetMap, helmetLore, EntityType.PILLAGER, 962265, 54173);
            for (String key : configSection.getKeys(false)) {
                PersistentDataContainer ps = event.getEntity().getPersistentDataContainer();
                if (ps.has(namespacedKey, PersistentDataType.INTEGER) && ps.get(namespacedKey, PersistentDataType.INTEGER).equals(configSection.getInt(key + ".UniqueIDNo"))) {
                    event.getDrops().add(new ItemStack(Material.valueOf(configSection.getString(key + ".BlockDropped")), configSection.getInt(key + ".Drop_Block")));
                }
            }
        }
    }

    @EventHandler
    public void onDeathEventForSumo(PlayerDeathEvent event) {
        if (ArenaManager.getManager().isInGame(event.getEntity().getPlayer())) {
            ChatMsgUtil.sendActionBar(event.getEntity(), "&3You Lost :(");
            ChatMsgUtil.sendTitle(event.getEntity(), "&3You Lost", "&3Try again next time!");
            ArenaManager.getManager().removePlayer(event.getEntity().getPlayer());
            Arena a = ArenaManager.getManager().getArenaFromPlayer(event.getEntity().getPlayer());
            try {
                Iterator<String> iterator = a.getPlayers().iterator();
                if (iterator.hasNext()) {
                    String pnames = iterator.next();
                    Player player1 = Bukkit.getPlayer(pnames);
                    ArenaManager.getManager().addWins(1, player1);
                    if (Bukkit.getServer().getPluginManager().getPlugin("Vault") != null && Bukkit.getServer().getPluginManager().getPlugin("Vault").isEnabled()) {
                        SmpEssentials.getEconomy().depositPlayer(player1, 50.0);
                    }
                    ChatMsgUtil.sendActionBar(player1, "&3YOU WON!");

                    ChatMsgUtil.sendTitle(player1, "&6Your Wins:", ArenaManager.getManager().getWins(player1));
                    ArenaManager.getManager().removePlayer(player1);
                }
            } catch (ConcurrentModificationException ex) {
                SmpEssentials.getPlugin(SmpEssentials.class).getLogger().severe(Arrays.toString(ex.getStackTrace()));
            }
        }
    }

    public void bosses(EntityDeathEvent event, NamespacedKey namespacedKey, PersistentDataContainer p, String itemDisplayName, Material customItemMaterial, Map<Enchantment, Integer> collection, ArrayList<String> lore,
                       EntityType entityType, int idGuardian, int idNoCustomItem) {
        if (event.getEntity().getType().equals(entityType)
                && p.has(namespacedKey, PersistentDataType.INTEGER)
                && p.get(namespacedKey, PersistentDataType.INTEGER).equals(idGuardian)) {
            event.getDrops().add(new ItemStack(Material.EMERALD_BLOCK, 90));
            ItemStack customsword = new ItemStack(customItemMaterial);
            customsword.addUnsafeEnchantments(collection);
            ItemMeta itemMeta = customsword.getItemMeta();
            itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', itemDisplayName));
            itemMeta.setLore(lore);
            NamespacedKey namespacedKeys = new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "ShrineBosses");

            PersistentDataContainer ps = itemMeta.getPersistentDataContainer();
            ps.set(namespacedKeys, PersistentDataType.INTEGER, idNoCustomItem);
            customsword.setItemMeta(itemMeta);
            event.getDrops().add(customsword);
            if (event.getEntity().getKiller() != null && event.getEntity().getKiller().getType() == EntityType.PLAYER) {
                event.getEntity().getKiller().setWalkSpeed(0.2f);

            }

        }
    }

    public boolean nullchecks(EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Player) {
            Player player = ((Player) event.getDamager()).getPlayer();
            if (player == null) return false;
            if (player.getInventory().getItemInMainHand().getType() == Material.AIR) return false;
            if (player.getInventory().getItemInMainHand().getItemMeta() == null) return false;
            if (player.getInventory().getItemInMainHand().getItemMeta().getLore() == null) return false;
            if (player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(null)) return false;
            return player.getInventory().getItemInMainHand().getItemMeta().hasDisplayName();
        }
        return false;
    }


}

