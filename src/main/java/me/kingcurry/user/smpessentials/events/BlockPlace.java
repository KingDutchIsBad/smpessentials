package me.kingcurry.user.smpessentials.events;

import me.kingcurry.user.smpessentials.ChatMsgUtil;
import me.kingcurry.user.smpessentials.SmpEssentials;
import me.kingcurry.user.smpessentials.guardian.Guardian;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.block.Container;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class BlockPlace implements Listener {
    @EventHandler
    public void above(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if (player.getItemInHand().getItemMeta() == null) return;
        if (player.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Backpack")) {
            event.setCancelled(true);
        }
        if (player.getItemInHand().getItemMeta() == null) return;
        if (player.getItemInHand().getItemMeta().getLore() == null) return;
        if (player.getItemInHand().getType() == Material.AIR) return;
        if (player.getItemInHand().getItemMeta().getLore().contains(null)) return;
        if (player.getItemInHand().getItemMeta().getLore().contains(org.bukkit.ChatColor.DARK_GREEN + "Portable Crafting Table")) {
            event.setCancelled(true);
        }
    }


    @EventHandler
    public void forCustomMobs(BlockBreakEvent event) {
        FileConfiguration config = SmpEssentials.getPlugin(SmpEssentials.class).getConfig();
        ConfigurationSection configSection = config.getConfigurationSection("CustomGuardian");
        if(configSection != null){
            for(String key : configSection.getKeys(false)){
                int math = (int) (Math.random() * configSection.getInt(key+".Chance_Spawning"));
                if(math == 1){
                    if(event.getBlock().getType().equals(Material.valueOf(configSection.getString(key + ".BlockRequired")))) {
                        LivingEntity entity = Guardian.getInstance().summonGuardian(event.getPlayer().getWorld(), event.getBlock().getLocation(), new ItemStack(Material.valueOf(configSection.getString(key + ".HELMET")))
                                , new ItemStack(Material.valueOf(configSection.getString(key + ".CHESTPLATE")))
                                , new ItemStack(Material.valueOf(configSection.getString(key + ".LEGGINS"))), new ItemStack(Material.valueOf(configSection.getString(key + ".BOOTS"))), EntityType.valueOf(configSection.get(key + ".EntityType").toString().toUpperCase())
                                , configSection.getInt(key + ".Damage"), configSection.getInt(key + ".Health"), 0.15, configSection.getString(key + ".Name"));
                        PersistentDataContainer p = entity.getPersistentDataContainer();
                        NamespacedKey namespacedKey = new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "Guardian");
                        p.set(namespacedKey, PersistentDataType.INTEGER, configSection.getInt(key + ".UniqueIDNo"));
                        String message = Objects.requireNonNull(configSection.getString(key + ".MessageWhenSpawned")).replace("%health%", "" + entity.getHealth());
                        ChatMsgUtil.sendMessage(event.getPlayer(), message);
                    }
                }
            }
        }
        if (SmpEssentials.getPlugin(SmpEssentials.class).getConfig().getBoolean("Telepathy Shovel.playeruse")) {
            Player player = event.getPlayer();
            // deepcode ignore ApiMigration~getBlock: DOESN'T EXIST
            Block block = event.getBlock();
            if (player.getInventory().getItemInMainHand().getType() == Material.AIR) return;
            if (player.getInventory().getItemInMainHand().getItemMeta() == null) return;
            if (player.getInventory().getItemInMainHand().getItemMeta().getLore() == null) return;
            if (block.getState() instanceof Container) return;
            if (player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(ChatColor.BLUE + "Telepathy Shovel")) {
                event.setDropItems(false);
                Collection<ItemStack> drops = block.getDrops(player.getInventory().getItemInMainHand());
                if (drops.isEmpty()) return;
                player.getInventory().addItem(drops.iterator().next());
            }
        }
        boolean enabled = true;
        if (!enabled) {
            ArrayList<String> lore = new ArrayList<String>();
            lore.add("This the is totem of the Villager God");
            lore.add("To Summon the Villager God place a emerald block");
            lore.add("and above that place a obsidian block");
            lore.add("and even above that place a bell then right click it with the totem");
            ArrayList<String> lore2 = new ArrayList<String>();
            lore2.add("This the is totem of the Sand Protector");
            lore2.add("To Summon the Sand Protector place a SandStone block");
            lore2.add("and above that place a obsidian block");
            lore2.add("and even above that place a bell then right click it with the totem");
            ArrayList<String> lore3 = new ArrayList<String>();
            lore3.add("This the is totem of the God Of Conflict and Wars");
            lore3.add("To Summon the God of Conflict and Wars place a SandStone block");
            lore3.add("and above that place a obsidian block");
            lore3.add("and even above that place a bell then right click it with the totem");
            forShrineBosses(event, (int) (Math.random() * 2), lore, Material.EMERALD_ORE, "The Totem of the Villager God", 898989);
            forShrineBosses(event, (int) (Math.random() * 2), lore2, Material.SANDSTONE, "The Totem of the Sand Protector", 121212);
            forShrineBosses(event, (int) (Math.random() * 2), lore3, Material.ICE, "The Totem on the God Of Conflicts and Wars", 54173);
        }
    }

    public void forShrineBosses(BlockBreakEvent event, int math2, ArrayList<String> arrayList, Material material, String displayName, int IdNo) {
        if (math2 == 1) {
            if (event.getBlock().getType().equals(material)) {
                Material type;
                ItemStack custom_totem = new ItemStack(type = Material.TOTEM_OF_UNDYING);
                ItemMeta itemMeta = custom_totem.getItemMeta();
                itemMeta.setLore(arrayList);
                itemMeta.setDisplayName(displayName);
                PersistentDataContainer p = itemMeta.getPersistentDataContainer();
                Plugin plugin;
                NamespacedKey namespacedKey = new NamespacedKey(plugin = SmpEssentials.getPlugin(SmpEssentials.class), "ShrineBosses");
                p.set(namespacedKey, PersistentDataType.INTEGER, IdNo);
                custom_totem.setItemMeta(itemMeta);
                event.getPlayer().getInventory().addItem(custom_totem);
            }
        }
    }
}
