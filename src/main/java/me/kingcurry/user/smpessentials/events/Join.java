package me.kingcurry.user.smpessentials.events;


import me.kingcurry.user.smpessentials.SmpEssentials;
import me.kingcurry.user.smpessentials.sumo.ArenaManager;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.List;

public class Join implements Listener {
    @EventHandler
    public void above(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        PersistentDataContainer dataContainer = player.getPersistentDataContainer();
        if (!dataContainer.has(new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "Backpack"), PersistentDataType.STRING)) {
            dataContainer.set(new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "Backpack"), PersistentDataType.STRING, "");
        }
        for (int i = 0; i < SmpEssentials.getPlayersThatJumped().size(); i++) {
            if (!player.isOp()) {
                List<Player> list = new ArrayList<Player>(SmpEssentials.getPlayersVanished());
                player.hidePlayer(SmpEssentials.getPlugin(SmpEssentials.class), list.get(i));
            }
        }
        if (SmpEssentials.get().getString(event.getPlayer().getUniqueId().toString()) == null) {
            SmpEssentials.get().set(event.getPlayer().getUniqueId().toString(), 0);
            SmpEssentials.save();
        }

    }

    @EventHandler
    public void leaveEvent(PlayerQuitEvent event) {
        if (ArenaManager.getManager().isInGame(event.getPlayer())) {
            ArenaManager.getManager().removePlayer(event.getPlayer());
        }
    }

}
