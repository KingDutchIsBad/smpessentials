package me.kingcurry.user.smpessentials.events;

import me.kingcurry.user.smpessentials.ChatMsgUtil;
import me.kingcurry.user.smpessentials.SmpEssentials;
import me.kingcurry.user.smpessentials.sumo.Arena;
import me.kingcurry.user.smpessentials.sumo.ArenaManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.UUID;

public class Walk implements Listener {
    @EventHandler
    public void onWalk(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (player.getInventory().getBoots() == null) {
            return;
        } else if (player.getInventory().getBoots().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&4Blood God Armour"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 40, 2));
        }
        if (player.getInventory().getLeggings() == null) {
            return;
        } else if (player.getInventory().getLeggings().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&4Blood God Armour"))) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 40, 2));
        }
        Location loc = event.getPlayer().getLocation();
        // deepcode ignore ApiMigration~getBlock: <The reason is because that thing isn't a thing in this guardian>
        Block locBlock = loc.getBlock();
        Material block = locBlock.getType();

        if (block == Material.WATER && ArenaManager.getManager().isInGame(player)) {
            Arena a = ArenaManager.getManager().getArenaFromPlayer(player);
            ArenaManager.getManager().removePlayer(player);
            ChatMsgUtil.sendTitle(player, "&3You lost :(", "&3Try Again next time!");
            try {
                Iterator<String> iterator = a.getPlayers().iterator();
                if (iterator.hasNext()) {
                    String pnames = iterator.next();
                    UUID uuid = Bukkit.getPlayer(pnames).getUniqueId();
                    Player player1 = Bukkit.getPlayer(pnames);
                    ArenaManager.getManager().addWins(1, player1);
                    if (Bukkit.getServer().getPluginManager().getPlugin("Vault") != null && Bukkit.getServer().getPluginManager().getPlugin("Vault").isEnabled()) {
                        SmpEssentials.getEconomy().depositPlayer(player1, 50.0);
                    }
                    ChatMsgUtil.sendActionBar(player1, "&3YOU WON!");

                    ChatMsgUtil.sendTitle(player1, "&6Your Wins:", SmpEssentials.get().getString(uuid.toString()));
                    ArenaManager.getManager().removePlayer(player1);
                }
            } catch (ConcurrentModificationException ex) {
                SmpEssentials.getPlugin(SmpEssentials.class).getLogger().severe(Arrays.toString(ex.getStackTrace()));
            }

        }
        if (ArenaManager.getManager().isInGame(player)) {
            Arena arena = ArenaManager.getManager().getArenaFromPlayer(player);
            if (arena.getPlayers().size() == 1) {
                event.setCancelled(true);
                ChatMsgUtil.sendActionBar(player, "&3Please wait while someone else joins");
            }

        }
    }
}


