package me.kingcurry.user.smpessentials.events;

import me.kingcurry.user.smpessentials.SmpEssentials;
import me.kingcurry.user.smpessentials.guardian.Guardian;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

public class Interact implements Listener {

    @EventHandler
    public void above(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        ItemMeta itemMetaHand = player.getInventory().getItemInMainHand().getItemMeta();
        if (itemMetaHand == null) return;
        if (event.getItem() == null) return;
        if (!player.getInventory().getItemInMainHand().getItemMeta().hasDisplayName()) return;
        if (player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.DARK_RED + "God AXE")) {
            @SuppressWarnings("unchecked")
            Location location = player.getTargetBlock(null, 3000).getLocation();
            if (location.getWorld() != null)
                location.getWorld().strikeLightning(location);
        }

        if (event.getClickedBlock() != null &&
                player.getInventory().getItemInMainHand().getEnchantments().containsKey(Enchantment.getByKey(SmpEssentials.getCustomEnchantments().getKey()))) {
            Location location = event.getClickedBlock().getLocation();
            if (location.getWorld() == null) return;
            location.getWorld().createExplosion(location, 4f);
        }


        if (itemMetaHand.hasDisplayName()
                && itemMetaHand.getDisplayName().equalsIgnoreCase(ChatColor.GOLD + "Backpack")) {
            player.chat("/backpack");
        }

        if (nullchecks(event)) {
            if (itemMetaHand.getLore().contains(ChatColor.DARK_AQUA + "Launcher which launches you high up in the air")) {
                event.getPlayer().setVelocity(new Vector(1.0, 3.0, 1.0));
                if (!SmpEssentials.getPlayersThatJumped().contains(event.getPlayer())) {
                    SmpEssentials.getPlayersThatJumped().add(event.getPlayer());
                }
            }
        }
        if (SmpEssentials.getPlugin(SmpEssentials.class).getConfig().getBoolean("Crafting Table.playeruse")
                && nullchecks(event)) {

            if (itemMetaHand.getLore().contains(ChatColor.DARK_GREEN + "Portable Crafting Table")) {
                player.openWorkbench(null, true);
            }
        }
        boolean enabled = true;
        if (!enabled) {
            FileConfiguration config = SmpEssentials.getPlugin(SmpEssentials.class).getConfig();
            ConfigurationSection configSection = config.getConfigurationSection("CustomGuardian");
            Plugin plugin;
            NamespacedKey namespacedKey2 = new NamespacedKey(plugin = SmpEssentials.getPlugin(SmpEssentials.class), "ShrineBosses");
            forShrineBosses(event, namespacedKey2, 898989,
                    ChatColor.translateAlternateColorCodes('&', "&4This is the first time someone has summoned me to kill me but I'll kill you first" + event.getPlayer().getName()), Material.EMERALD_BLOCK, 12216,
                    new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), EntityType.ILLUSIONER, 8, 150, 0.4, "&6The Villager God");
            forShrineBosses(event, namespacedKey2, 121212, ChatColor.translateAlternateColorCodes('&', "&4Again? Why did you summon me this is like the 100th time someone has summoned me with the intent to kill me. Goodbye" + event.getPlayer().getName()), Material.SANDSTONE, 3341212,
                    new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), EntityType.SKELETON, 3, 200, 0.1, "&6The Sand Protector");
            forShrineBosses(event, namespacedKey2, 54173, ChatColor.translateAlternateColorCodes('&', "&4Well, now that you have summoned me prepare to die" + event.getPlayer().getName()), Material.BELL, 962265,
                    new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), EntityType.PILLAGER, 20, 100, 0.3, "&6The God of Conflict and War");

        }
    }

    public boolean nullchecks(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (event.getItem() == null) return false;
        if (player.getInventory().getItemInMainHand().getType() == Material.AIR) return false;
        if (player.getInventory().getItemInMainHand().getItemMeta() == null) return false;
        if (player.getInventory().getItemInMainHand().getItemMeta().getLore() == null) return false;
        if (player.getInventory().getItemInMainHand().getItemMeta().getLore().contains(null)) return false;
        return player.getInventory().getItemInMainHand().getItemMeta().hasDisplayName();
    }



    public void forShrineBosses(PlayerInteractEvent event, NamespacedKey namespacedKey2, int idNo, String messageSent, Material downMaterial, int idGuardian,
                                ItemStack helmet, ItemStack chestplate, ItemStack
                                        leggings, ItemStack boots, EntityType mobType, double attackDamage, double health, double movementSpeed, String name) {
        Block block = event.getClickedBlock();
        if (block != null && block.getType().equals(Material.BELL)
                && block.getRelative(BlockFace.DOWN).getType().equals(Material.OBSIDIAN)
                && block.getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).getType().equals(downMaterial)
                && nullchecks(event)
                && event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getPersistentDataContainer().has(namespacedKey2, PersistentDataType.INTEGER)
                && event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getPersistentDataContainer().get(namespacedKey2, PersistentDataType.INTEGER).equals(idNo)) {
            LivingEntity entity = Guardian.getInstance().summonGuardian(event.getPlayer().getWorld(), event.getPlayer().getLocation(), helmet, chestplate, leggings
                    , boots, mobType, attackDamage, health, movementSpeed, ChatColor.translateAlternateColorCodes('&', name));
            block.setType(Material.AIR);
            block.getRelative(BlockFace.DOWN).setType(Material.AIR);
            block.getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).setType(Material.AIR);
            event.getPlayer().sendMessage(messageSent);
            NamespacedKey namespacedKey = new NamespacedKey(SmpEssentials.getPlugin(SmpEssentials.class), "ShrineBosses");
            PersistentDataContainer p = entity.getPersistentDataContainer();
            p.set(namespacedKey, PersistentDataType.INTEGER, idGuardian);
            if (idGuardian == 962265) {
                for (int i = 0; i <= 30; i++) {
                    entity.getWorld().spawnEntity(entity.getLocation(), EntityType.PILLAGER);
                }

            }
        }
    }
}