package me.kingcurry.user.smpessentials.guardian;


import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

public class Guardian {
    private static Guardian cool = null;

    protected Guardian() {
    }

    public static Guardian getInstance() {
        if (cool == null) {
            cool = new Guardian();
        }
        return cool;
    }

    public LivingEntity summonGuardian(World world, Location location, ItemStack helmet, ItemStack chestplate, ItemStack
            leggings, ItemStack boots, EntityType mobType, double attackDamage, double health, double movementSpeed, String name) {
        name = ChatColor.translateAlternateColorCodes('&', name);
        LivingEntity Guardian = (LivingEntity) world.spawnEntity(location, mobType);
        if (helmet != null) Guardian.getEquipment().setHelmet(helmet);
        if (chestplate != null) Guardian.getEquipment().setChestplate(chestplate);
        if (leggings != null) Guardian.getEquipment().setLeggings(leggings);
        if (boots != null) Guardian.getEquipment().setBoots(boots);
        Guardian.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(attackDamage);
        Guardian.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
        Guardian.setHealth(health);
        Guardian.getAttribute(Attribute.GENERIC_MOVEMENT_SPEED).setBaseValue(movementSpeed);
        Guardian.setCustomName(name);
        return Guardian;
    }
}
