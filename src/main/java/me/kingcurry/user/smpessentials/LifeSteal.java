package me.kingcurry.user.smpessentials;

import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;

public class LifeSteal extends BukkitRunnable {
    LivingEntity victim;
    LivingEntity damager;
    int timer = 3;

    public LifeSteal(LivingEntity victim, LivingEntity damager) {
        this.victim = victim;
        this.damager = damager;
    }

    @Override
    public void run() {
        if (victim.getHealth() > 1 && damager.getHealth() < 19 && timer > 1) {
            // victim.setHealth(victim.getHealth() -1);
            damager.setHealth(damager.getHealth() + 1);
            timer--;
            victim.damage(1);
        } else if (victim.getHealth() == 0 || damager.getHealth() == 0 || timer == 0) {
            cancel();
        }

    }
}
